let celebrity = {
	fullName: "Kevin",
	lastName: "Sorbo",
	age: 63,
	stageName: "Kevin",
	birthday: "September 24, 1958",
	bestShow: "Hercules",
	bestMovie: "Andromeda",
	isActive: true,
	};

console.log(celebrity);


// Operators 

// Artihmetic Operators
	
	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: "+sum);
	//result:9228

	let difference = x - y;
	console.log("Result of subtraction oeprator: " + difference)
	//result: -6434

	let product = x * y;
	console.log("Result of multiplicaiton operator: " + product);
	//result: 10939907

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);
	//result: 0.17839356404035245

	let remainder = y % x;
	console.log("Result of modulo operator"+ remainder);
	//result: 846

// Assignment Operator 
	// Basic Assignment Operator (=)

	let assignmentNumber = 8; 

	// Shorthand arithmetic operators

	// Addition Assignment Operator (+=)
	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assignment: " + assignmentNumber); //result: 10

	assignmentNumber += 2;
	console.log("Result of addition assignment: " + assignmentNumber); //result: 12

	let string1 = "Boston";
	let string2 = "Celtics";

	string1 += string2;
	console.log(string1); //result: BostonCeltics

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber); //result: 10

	string1 -= string2
	console.log(string1); //result: NaN (Not a Number)

	/* 
		Mini-Activity
		>> Using the shorthand method, perform the following operations on the assignment number
			1. multiplication
			2. division
		>> Print the individual output in your console
	*/

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);
	//result: 20

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);
	//result: 10

//Multiple Operators and Parentheses

	let mdas = 1 + 2 - 3 * 4 / 5 
	console.log("Result of mdas: "+mdas);
	//result: 0.6000000000000001
	/*
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas: " + pemdas)
	//result: 0.2
	/*
		1. 4 / 5 = 0.8
		2. 2 - 3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = 0.2
	*/

	// Increment and Decrement

		// pre-increment and post-increment
		let z = 1; 

		let increment = ++z;
		console.log("Result of pre-increment: " + increment)
		//result: 2
		console.log("Result of pre-increment: " + z);
		//result: 2

		increment = z++; 
		console.log("Result of post-increment: " + increment);
		//result: 2
		console.log("Result of post-increment " + z);
		//result: 3

		let decrement = --z;
		console.log("Result of pre-decrement: " + decrement);
		//result: 2
		console.log(z); 
		//result: 2

		decrement = z--; 
		console.log("Result of post-decrement: " + decrement);
		//result: 2
		console.log(z);
		//result: 1

// Type Coercion

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log("Result of coercion: " + coercion);
// result: 1012
console.log(typeof coercion);
// result: string

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log("Non-Coercion: " + nonCoercion);
// result: 30
console.log(typeof nonCoercion);
// result:number

let numE = true + 1;
// true === 1
// 1 + 1 = 2
console.log(numE);
// result: 2

let numF = false + 1;
//false === 0
//0 + 1
console.log(numF);
//result: 1

//Comparison Operators

	// Equality Operator (==)

	let anya = "anya";

	console.log("anya" == "anya")
	// result: true

	console.log("anya" == anya)
	// result: true

	console.log(1==false);
	//result: false
	console.log(1 == true);
	//result: true
	console.log("false" == false);
	//result : false

	// Loose Inequal Operator (!=)

	console.log(1 !=1);
	//result: false
	console.log("1" !=1);
	//result: false
	console.log(anya != "anya");
	//result: false
	console.log(1 != 2);
	//result: true

	// Strict Equality Opetarot (===)

	console.log(1 === 1);
	//result: true
	console.log("1" === 1);
	//result: false
	console.log(1 === false);
	//result: false

	//Strict Inequaltiy Operator (!==)

	console.log("1" !== true);
	//result: true
	console.log("Anya" !== "anya");
	//result: true

// Relational Operators

let a = 50;
let b = 65

// GT or greater than operator (>)
let isGreaterThan = a > b
console.log(isGreaterThan)
// result: false

// LT or less than operator (<)
let isLessThan = a < b;
console.log(isLessThan);
// result: true

// GTE and LTE or Greater Than or Equal and Less Than or Equal (>= or <=)

let isGTE = a >= b;
let isLTE = a <= b;

console.log(isGTE, isLTE);
//result: false, true

let numStr = "30";
console.log(a > numStr); //result: true - forced type coercion to change the string to number

let str = "twenty";
console.log(b >= str); //result: false - 65 >= NaN

//Logical Operators

	 let isLegalAge = true;
	 let isRegistered = false;

	 // AND operator (&& - doulbe ampersand)
	 	// returns true if ALL operands are true

	 let allRequirementsMet = isLegalAge && isRegistered;
	 console.log(allRequirementsMet);
	 //result: false

	 // OR Operator (|| - double pipe)
	 	// Returns true IF oe=ne of the operands are true

	 let someRequirementsMet = isLegalAge || isRegistered;
	 console.log(someRequirementsMet);
	 //result: true

	 // NOT Operator (!)
	 	// turns a boolean into opposite value
	 let someRequirementsNotMet = !isRegistered;
	 console.log(someRequirementsNotMet);
	 // result: true

// Multiple Operators

	let requiredLevel = 95
	let requiredAge = 18 

	let authorization1 = isRegistered && requiredLevel === 25;
	console.log(authorization1);
	// result: false

	let authorization2 = isRegistered && isLegalAge && requiredLevel === 95
	console.log(authorization2)
	// result: false
	
		let authorization3 = !isRegistered && isLegalAge && requiredLevel === 95
	console.log(authorization3)
	// result: true